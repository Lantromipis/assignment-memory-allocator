//
// Created by Lantromipis on 31.01.2021.
//

#include "mem.h"
#include "mem_internals.h"
#include "mem_debug.h"

void start_test_pint(int test_num, char* const text){
    printf("______________Starting Test №%d, please stand by______________\n%s\n ", test_num, text);
}

int main(){
    start_test_pint(1, "Initialization of heap");
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);

    start_test_pint(2, "Normal memory allocation success.");
    void* block1 = _malloc(1);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    _malloc(1000);
    _malloc(1000);
    debug_heap(stdout, heap);

    start_test_pint(3, "Freeing one block from several allocated ones.");
    _free(block1);
    debug_heap(stdout, heap);

    start_test_pint(4, "Freeing two blocks from several allocated ones.");
    _free(block2);
    _free(block3);
    debug_heap(stdout, heap);

    start_test_pint(5, "The memory has run out, the new memory region expands the old one.");
    _malloc(10000);
    _malloc(10000);
    debug_heap(stdout, heap);

    start_test_pint(6, "The memory has run out, the old memory region cannot be expanded due to a different allocated range of addresses, the new region is allocated in a different place.");
    _malloc(134217711);
    _malloc(134217711);
    debug_heap(stdout, heap);

    free_heap();
}